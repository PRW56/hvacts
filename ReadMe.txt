HVACTS 

(HVAC Training Simulator)

How to Run (note that a Vive headset is required to run the project):
1. Download and Install Unity version 5.5.1f1.
2. Download and Install Steam and SteamVR.
3. Set up your vive headset if you have not already done so (follow the instructions 
provided with the headset), otherwise make sure the headset is plugged in and tracking
correctly on SteamVR.
4. Open the project in unity (This will open SteamVR if it is not already open), 
and open mainScene located in Scenes folder.
5. The project can be run by clicking the play button at the top middle of the screen.

How to use:
1. After launching the project, click continue on the disclaimer.
2. You will now be at the main menu, to your right is the level select screen. Select a 
level by pointing at it with the controller that has a laser pointer and click the trigger
to select a level (note that the top two images are tutorial levels, the bottom 2 are 
scenario levels).
3. After selecting a level, turn to the door and click the start button in the same way.
The level you selected will load. After completing it you will be returned to the main
menu.

How to Build an Exe:
1. Follow the steps in "How to Run" to set up the project.
2. With the project open in Unity, click File -> Build Settings, then click Build.
3. Choose a location for the exe, click save, and let unity build the project.
4. Exe will be generated in the specified location
