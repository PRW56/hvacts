﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHand : MonoBehaviour {

	Rigidbody attachPoint;
	SteamVR_TrackedObject trackedObj;
	FixedJoint joint = null;

	bool setup = false;
	bool holding = false;
	bool triggerClickHandled = false; //used to determine if a trigger click has already held/dropped something
	GameObject touchedObject = null;
	
	// Use this for initialization
	void Start () {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		
	}

	//start on right controller,
	void Test(object sender, PointerEventArgs e)
	{
		Debug.Log(name + " boom");
	}

	// Update is called once per frame
	void Update () {

		//don't do anything until all the VR stuff gets initialized
		
		if (setup == false)
		{
			Transform tip = transform.GetChild(0).FindChild("tip");
			if (tip != null)
			{
				tip = tip.GetChild(0);
				attachPoint = tip.gameObject.AddComponent<Rigidbody>();
				attachPoint.isKinematic = true;
				setup = true;
			}
			else
				return;
		}

		//if the controller is colliding with a grabable object, check if it should be picked up
		var device = SteamVR_Controller.Input((int)trackedObj.index);

		if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger) && triggerClickHandled == false)
		{

			if (joint == null && touchedObject != null)
			{
				touchedObject.transform.position = attachPoint.transform.position;
				Grabable grab = touchedObject.GetComponent<Grabable>();
				if (grab.customPlacement)
				{
					touchedObject.transform.position = attachPoint.transform.position + grab.customPosDelta;
					touchedObject.transform.eulerAngles = transform.eulerAngles + grab.customRotDelta;
				}

				joint = touchedObject.AddComponent<FixedJoint>();
				joint.connectedBody = attachPoint;
				holding = true;
				grab.holdingHand = device;
				touchedObject.GetComponent<Grabable>().PickUp();
			}
			else if (joint != null)
			{
				Rigidbody rigidbody = joint.gameObject.GetComponent<Rigidbody>();
				Grabable grab = joint.gameObject.GetComponent<Grabable>();
				Object.DestroyImmediate(joint);
				joint = null;

				if (grab.Drop() == false)
				{
					var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
					if (origin != null)
					{
						rigidbody.velocity = origin.TransformVector(device.velocity);
						rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
					}
					else
					{
						rigidbody.velocity = device.velocity;
						rigidbody.angularVelocity = device.angularVelocity;
					}
					rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
				}
				
				holding = false;
				grab.holdingHand = null;
			}

			triggerClickHandled = true;
		}

		if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
			triggerClickHandled = false;
		/*
		if (joint == null && touchedObject != null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			
			touchedObject.transform.position = attachPoint.transform.position;
			Grabable grab = touchedObject.GetComponent<Grabable>();
			if (grab.customPlacement)
			{
				touchedObject.transform.position = attachPoint.transform.position + grab.customPosDelta;
				touchedObject.transform.eulerAngles = transform.eulerAngles + grab.customRotDelta;
			}

			joint = touchedObject.AddComponent<FixedJoint>();
			joint.connectedBody = attachPoint;
			holding = true;
		}
		else if(joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			Rigidbody rigidbody = joint.gameObject.GetComponent<Rigidbody>();
			Object.DestroyImmediate(joint);
			joint = null;

			var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
			if (origin != null)
			{
				rigidbody.velocity = origin.TransformVector(device.velocity);
				rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
			}
			else
			{
				rigidbody.velocity = device.velocity;
				rigidbody.angularVelocity = device.angularVelocity;
			}

			rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
			holding = false;
		}*/
	}

	void OnTriggerEnter(Collider other)
	{
		Grabable grab = other.gameObject.GetComponent<Grabable>();
		if (holding == true || grab == null || grab.enabled == false)
			return;
		//if (touchedObject != null)
			//other.GetComponent<MeshRenderer>().material.color = Color.grey;
		//other.GetComponent<MeshRenderer>().material.color = Color.blue;
		touchedObject = other.gameObject;
	}

	void OnTriggerExit(Collider other)
	{
		Grabable grab = other.gameObject.GetComponent<Grabable>();
		if (holding == true || grab == null || grab.enabled == false)
			return;
		if(other.gameObject == touchedObject)
		{
			//other.GetComponent<MeshRenderer>().material.color = Color.grey;
			touchedObject = null;
		}
	}
}
