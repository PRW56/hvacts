﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotLight : Part
{
	ParticleSystem fire;

	bool burning;
	public const float gasTime = 5f;
	float gasTimer = gasTime;
	// Use this for initialization
	void Start () {
		fire = transform.GetChild(0).GetComponent<ParticleSystem>();
		AddRelatedPart("GasSwitch");
		if(Player.currentLevelIndex == 0)
		{
			ToggleFlame();
			ToggleFlame(); //Don't understand why twice, but must be twice to work
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(burning)
		{
			if(gasTimer > 0)
			{
				if (((GasSwitch)relatedParts["GasSwitch"]).gasOn == false)
				{
					Respond("GasOffEarly");
					return;
				}
				gasTimer -= Time.deltaTime;
				if (gasTimer <= 0)
					Respond("BurnTimeReached");
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "LighterTip")
		{
			ParticleSystem system = other.GetComponent<ParticleSystem>();
			if(system != null && system.emission.enabled)
				Respond("Lit");
		}
	}

	public override void Respond(object cause)
	{
		switch ((string)cause)
		{
			case "Lit":
				gasTimer = gasTime;
				burning = true;
				ToggleFlame();
				break;
			case "GasOffEarly":
				burning = false;
				ToggleFlame();
				break;
			case "BurnTimeReached":

				break;
		}
		InformInstructor((string)cause);
	}

	void ToggleFlame()
	{
		ParticleSystem.EmissionModule em = fire.emission;
		em.enabled = !em.enabled;
		fire.Play();
	}
}
