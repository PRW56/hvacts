﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserHand : MonoBehaviour {
	
	SteamVR_TrackedObject trackedObj;
	SteamVR_LaserPointer laser;
	static LaserHand currentHand;
	Button selectedButton;

	bool setup = false;

	// Use this for initialization
	void Start() {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		laser = GetComponent<SteamVR_LaserPointer>();

		Debug.Log(trackedObj.index);
		
	}
	
	// Update is called once per frame
	void Update () {
		if (setup == false)
		{
			if (trackedObj.index == SteamVR_TrackedObject.EIndex.Device4)
			{
				currentHand = this;
				laser.PointerIn += PointerIn;
				laser.PointerOut += PointerOut;
			}
			else
			{
				laser.pointer.SetActive(false);
			}
			setup = true;
		}

		var device = SteamVR_Controller.Input((int)trackedObj.index);

		if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			if (currentHand != this)
				SwitchLaserHand();
			else if (selectedButton != null)
				selectedButton.onClick.Invoke();
		}
	}

	void PointerIn(object sender, PointerEventArgs e)
	{
		Button b = e.target.GetComponent<Button>();
		if (b != null)
		{
			e.target.GetComponent<Image>().color = b.colors.highlightedColor;
			selectedButton = b;
			
		}
	}

	void PointerOut(object sender, PointerEventArgs e)
	{
		Button b = e.target.GetComponent<Button>();
		if (b != null)
		{
			e.target.GetComponent<Image>().color = b.colors.normalColor;
			selectedButton = null;
		}
	}

	void SwitchLaserHand()
	{
		//unsub the other hand's even handlers
		currentHand.laser.PointerIn -= currentHand.PointerIn;
		currentHand.laser.PointerOut -= currentHand.PointerOut;

		//hide other laser
		currentHand.laser.pointer.SetActive(false);

		//sub my event handlers
		laser.PointerIn += PointerIn;
		laser.PointerOut += PointerOut;

		//show my laser
		laser.pointer.SetActive(true);

		currentHand = this;
	}
}
