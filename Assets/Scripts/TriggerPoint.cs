﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPoint : MonoBehaviour {

	//reference to object that can be placed in the associated placement area
	PlacementArea placementArea;

	// Use this for initialization
	void Start () {
		placementArea = transform.parent.GetComponent<PlacementArea>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Grabable>() == null || other.GetComponent<Rigidbody>().isKinematic)
			return;
		foreach(Grabable obj in placementArea.placeableObjects)
		{
			if (other.gameObject == obj.gameObject)
			{
				placementArea.EnterTrigger(obj);
				return;
			}
		}		
	}

	void OnTriggerExit(Collider other)
	{
		Grabable grab = other.gameObject.GetComponent<Grabable>();
		if (grab != null)
			placementArea.ExitTrigger(grab);
	}
}
