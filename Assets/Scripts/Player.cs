﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//placed on an empty game object to keep track of player info
public class Player : MonoBehaviour {

	int points;
	Level[] levels;
	bool[] completedTutorials; //no associated score, so only completion status needed (for now)

	public static int currentLevelIndex;
	public static bool isTutorial = true;
	public static bool isDebug = true;


	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangePoints(int delta, string cause)
	{
		points += delta;

		//display cause

	}
}
