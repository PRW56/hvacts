﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasSwitch : Part
{
	Transform button;
	public bool gasOn;

	SteamVR_TrackedObject controller;

	float depressionAmmount = .1f;

	// Use this for initialization
	void Start () {
		button = transform.parent.GetChild(1);
		GetComponent<VRSwitch>().OnSwitchTriggered += ToggleGas;
		gasOn = false;
		controller = null;
		AddRelatedPart("GasSettingSwitch");
	}
	
	// Update is called once per frame
	void Update () {
		if(gasOn)
		{
			var device = SteamVR_Controller.Input((int)controller.index);
			//check if button is still held down
			if(device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
			{
				gasOn = false;
				button.localPosition += Vector3.up * depressionAmmount;
				Respond("GasOff");
				return;
			}
		}
	}

	void ToggleGas(SteamVR_TrackedObject controller)
	{
		if(((GasSettingSwitch)relatedParts["GasSettingSwitch"]).isPilotSetting == false)
		{
			Respond("NonPilotGasOn");
			return;
		}
		gasOn = true;
		button.localPosition -= Vector3.up * depressionAmmount;
		this.controller = controller;
		Respond("GasOn");
	}

	public override void Respond(object cause)
	{
		switch ((string)cause)
		{
			case "GasOn":

				break;
			case "GasOff":

				break;
			case "NonPilotGasOn":

				break;
		}
		InformInstructor((string)cause);
	}
}
