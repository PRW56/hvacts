﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {
	public string name;
	int topScore;
	bool completed;

	public Level(string name)
	{
		this.name = name;
		topScore = 0;
		completed = false;
	}

	public void Complete(bool win, int score)
	{
		if (topScore < score)
			topScore = score;
		if (win)
			completed = true;
	}
}
