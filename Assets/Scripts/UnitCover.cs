﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCover : Part {

	Grabable grab;

	// Use this for initialization
	void Start () {
		//find all the other cover screws
		for (int x = 1; x <= 4; x++)
		{
			AddRelatedPart("CoverScrew" + x);
		}
		//find air filter
		AddRelatedPart("UsedAirFilter");

		grab = GetComponent<Grabable>();
		grab.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void Respond(object cause)
	{
		switch((string)cause)
		{
			case "Unlock":
				grab.enabled = true;
				break;
			case "FullLock":
			case "Lock":
				grab.enabled = false;
				break;
			case "SnappedIn":
				//make it possible to screw in screws
				for (int x = 1; x <= 4; x++)
					relatedParts["CoverScrew" + x].enabled = true;
				//make things inside not movable
				if (relatedParts["UsedAirFilter"].GetComponent<Rigidbody>().isKinematic)
					relatedParts["UsedAirFilter"].GetComponent<Grabable>().enabled = false;
				break;
			case "SnappedOut":
				//prevent screwing in screws
				for (int x = 1; x <= 4; x++)
					relatedParts["CoverScrew" + x].enabled = false;
				//make things inside movable
				if (relatedParts["UsedAirFilter"].GetComponent<Rigidbody>().isKinematic)
					relatedParts["UsedAirFilter"].GetComponent<Grabable>().enabled = true;
				break;
			default:
				print(name + " unrecognized Response cause: " + (string)cause);
				break;
		}
		InformInstructor((string)cause);
	}
}
