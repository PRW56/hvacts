﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementArea : MonoBehaviour {

	public bool snappedInAtStart;
	public int snappedInAtStartIndex;
	public List<Grabable> placeableObjects;
	Grabable currentObject; //the placeable object that is currently being set off by the triggers
	int numTriggers;
	public int triggerCounter;
	bool snapInOnDrop;


	// Use this for initialization
	void Start () {
		numTriggers = transform.childCount;
		triggerCounter = 0;
		snapInOnDrop = false;
		currentObject = null;

		if (snappedInAtStart)
		{
			currentObject = placeableObjects[snappedInAtStartIndex];
			placeableObjects[snappedInAtStartIndex].OnPickedUp += SnapOut;
			placeableObjects[snappedInAtStartIndex].GetComponent<Rigidbody>().isKinematic = true;
			GetComponent<MeshRenderer>().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void EnterTrigger(Grabable cause)
	{
		if (currentObject == null)
			currentObject = cause;
		else if (currentObject != cause)
			return;
		//print("trigger up: " + cause.name);
		triggerCounter++;
		if(triggerCounter >= numTriggers)
		{
			//print("subbed");
			snapInOnDrop = true;
			currentObject.OnDropped += SnapIn;
		}
		if (triggerCounter > numTriggers)
			print(name + ": more triggers were hit than exist");
	}

	public void ExitTrigger(Grabable cause)
	{
		//ignore any other objects the player attempts to place, if they are already trying to place another one
		if (currentObject == null || currentObject != cause)
			return;
		triggerCounter--;
		if (snapInOnDrop)
		{
			//print("UNsubbed");
			currentObject.OnDropped -= SnapIn;
		}
		snapInOnDrop = false;
		if (triggerCounter == 0)
			currentObject = null;
		if(triggerCounter < 0)
			print(name + ": more triggers were removed than exist");
	}

	void SnapIn()
	{
		//move the associated object into position
		currentObject.transform.position = this.transform.position;

		//rotate object
		currentObject.transform.rotation = this.transform.rotation;

		//let part know
		Part part = currentObject.gameObject.GetComponent<Part>();
		if (part != null)
			part.Respond("SnappedIn");

		//make sure the part stays in place
		part.GetComponent<Rigidbody>().isKinematic = true;

		//subscribe to grabbed event handler
		currentObject.OnPickedUp += SnapOut;

		//make the placement area invisible
		GetComponent<MeshRenderer>().enabled = false;
	}

	void SnapOut()
	{
		//let part know
		Part part = currentObject.gameObject.GetComponent<Part>();
		if (part != null)
			part.Respond("SnappedOut");
		
		part.GetComponent<Rigidbody>().isKinematic = false;

		//unsub
		currentObject.OnPickedUp -= SnapOut;
		currentObject = null;

		//make the placement area visible
		GetComponent<MeshRenderer>().enabled = true;
		triggerCounter = 0;
	}
}
