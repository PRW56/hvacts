﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Any object that has this script and a collider can be picked up, script must be enabled to be grabable
/// </summary>
public class Grabable : MonoBehaviour {
	public bool customPlacement;
	public Vector3 customPosDelta;
	public Vector3 customRotDelta;

	//currently used to alert a placement area when the object is dropped
	public delegate void DroppedEventHandler();
	public event DroppedEventHandler OnDropped;

	//currently used to alert a placement area when the object is picked up
	public delegate void PickedUpEventHandler();
	public event PickedUpEventHandler OnPickedUp;

	public SteamVR_Controller.Device holdingHand;

	void Start()
	{

	}

	public bool Drop()
	{
		if (OnDropped == null)
			return false;
		OnDropped();
		return true;
	}

	public bool PickUp()
	{
		if (OnPickedUp == null)
			return false;
		OnPickedUp();
		return true;
	}

	//gonna need this later
	//public bool held;
}
