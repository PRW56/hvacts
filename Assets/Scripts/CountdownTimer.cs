﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {

    public float myCDTimer = 10;
    private Text ClockText;

	// Use this for initialization
	void Start () {
        ClockText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (myCDTimer > 0)
        {
            myCDTimer -= Time.deltaTime;
            ClockText.text = "Time Left: " + myCDTimer.ToString("f2") + " s";
            //print(myCDTimer);
        }
        else
        {
            myCDTimer = 0;
            ClockText.text = "Time Left: " + myCDTimer.ToString("f2") + " s";
            //print(myCDTimer + " s");
        }
        if(myCDTimer < 10)
        {
            ClockText.color = Color.red;
        }
	}
}
