﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {

	string selectedLevelName;

	// Use this for initialization
	void Start () {
        //selectedLevelName = "Level";
        selectedLevelName = "mainScene";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SelectLevel(string name)
	{
		if (name.Contains("Tutorial"))
			Player.isTutorial = true;
		else
			Player.isTutorial = false;
		if (name.Contains("1"))
			Player.currentLevelIndex = 0;
		else
			Player.currentLevelIndex = 1;
		selectedLevelName = name;
        //Debug.Log("Selected " + selectedLevelName);
    }

	public void LoadLevel()
	{
        //Debug.Log("Loading " + selectedLevelName);
		SceneManager.LoadScene("Level");
	}

    public void Restart()
    {
        //Debug.Log("Restarting " + selectedLevelName);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void EndGame()
    {
        //Debug.Log("Leaving Game");
        Application.Quit();
    }
}
