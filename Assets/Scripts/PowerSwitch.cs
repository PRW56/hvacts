﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//expects VRSwitch script
public class PowerSwitch : Part {
	public bool isPowerOn;
	MeshRenderer flipper;

	// Use this for initialization
	void Start () {
		flipper = transform.parent.FindChild("SwitchFlipper").GetComponent<MeshRenderer>();
		UpdateColor();
		GetComponent<VRSwitch>().OnSwitchTriggered += FlipPower;
	}

	void UpdateColor()
	{
		if (isPowerOn)
			flipper.material.color = Color.green;
		else
			flipper.material.color = Color.red;
	}

	void FlipPower(SteamVR_TrackedObject controller)
	{
		isPowerOn = !isPowerOn;
		transform.parent.eulerAngles += Vector3.right * 180;
		UpdateColor();
		if (isPowerOn)
			Respond("PowerOn");
		else
			Respond("PowerOff");
	}

	public override void Respond(object cause)
	{
		switch ((string)cause)
		{
			case "PowerOn":
				
				break;
			case "PowerOff":
				
				break;
		}
		InformInstructor((string)cause);
	}
}
