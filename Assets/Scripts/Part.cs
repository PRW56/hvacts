﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Part : MonoBehaviour {
	//list of parts that could cause Response to be called, affect Respond, or are affected by Respond
	protected Dictionary<string, Part> relatedParts;

	public Part()
	{
		relatedParts = new Dictionary<string, Part>();
	}
	
	public abstract void Respond(object cause);

	protected void AddRelatedPart(string partName)
	{
		Part temp = GameObject.Find(partName).GetComponent<Part>();
		if(temp != null)
			relatedParts.Add(partName, temp);
		else
			print(this.name + " could not find related part: " + partName);
	}

	protected void InformInstructor(string cause)
	{
		Instructor.instance.Inform(name + ":" + cause);
	}
}
