﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour {

	Grabable grab;
	ParticleSystem tipFire;
	bool triggerClickHandled;
	bool firstTime;

	// Use this for initialization
	void Start () {
		grab = GetComponent<Grabable>();
		tipFire = transform.FindChild("LighterTip").GetComponent<ParticleSystem>();
		triggerClickHandled = false;
		firstTime = true;
	}

	// Update is called once per frame
	void Update () {
		if (grab.holdingHand != null && grab.holdingHand.GetTouchDown(SteamVR_Controller.ButtonMask.Grip) && triggerClickHandled == false)
		{
			if (firstTime)
			{
				ToggleFlame();
				firstTime = false;
			}
			ToggleFlame();
			triggerClickHandled = true;
		}
		else if (grab.holdingHand != null && grab.holdingHand.GetTouchUp(SteamVR_Controller.ButtonMask.Grip))
			triggerClickHandled = false;
	}

	void ToggleFlame()
	{
		ParticleSystem.EmissionModule em = tipFire.emission;
		em.enabled = !em.enabled;
		tipFire.Play();
	}
}
