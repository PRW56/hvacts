﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirFilter : Part {

	public bool isDirty;
	public Color dirtyColor;

	// Use this for initialization
	void Start () {
		if(isDirty && Player.currentLevelIndex == 0)
		{
			transform.GetChild(0).GetComponent<MeshRenderer>().material.color = dirtyColor;
			GetComponent<Grabable>().enabled = false;
		}

		//
	}

	public override void Respond(object cause)
	{
		switch ((string)cause)
		{
			case "SnappedIn":

				break;
			case "SnappedOut":

				break;
			default:
				print(name + " unrecognized Response cause: " + (string)cause);
				break;
		}
		InformInstructor((string)cause);
	}
}
