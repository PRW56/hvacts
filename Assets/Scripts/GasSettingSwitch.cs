﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GasSettingSwitch : Part
{
	public bool isPilotSetting;
	Text settingDisplay;

	public Vector3 rotated = new Vector3(270, .18f, 78.627f);
	public Vector3 initial;

	// Use this for initialization
	void Start () {
		GetComponent<VRSwitch>().OnSwitchTriggered += FlipSetting;
		isPilotSetting = false;
		settingDisplay = transform.parent.parent.FindChild("Canvas").GetChild(0).GetChild(0).GetComponent<Text>();
		initial = transform.parent.localEulerAngles;
	}

	void FlipSetting(SteamVR_TrackedObject controller)
	{
		isPilotSetting = !isPilotSetting;
		if (isPilotSetting)
		{
			Respond("SetPilot");
			settingDisplay.text = "Pilot";
			transform.parent.localEulerAngles = rotated;
		}
		else
		{
			Respond("SetNotPilot");
			settingDisplay.text = "Not Pilot";
			transform.parent.localEulerAngles = initial;
		}
			
	}

	public override void Respond(object cause)
	{
		switch ((string)cause)
		{
			case "SetPilot":

				break;
			case "SetNotPilot":

				break;
		}
		InformInstructor((string)cause);
	}
}
