﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void SwitchEventHandler(SteamVR_TrackedObject cause);

//fires an event when the controller touches it and pulls the trigger
//expects an attached trigger
public class VRSwitch : MonoBehaviour {

	public event SwitchEventHandler OnSwitchTriggered;
	SteamVR_TrackedObject touchedController;
	bool triggerClicked;

	// Use this for initialization
	void Start () {
		triggerClicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(touchedController != null)
		{
			var device = SteamVR_Controller.Input((int)touchedController.index);
			if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
			{
				if (OnSwitchTriggered != null)
					OnSwitchTriggered(touchedController);
				triggerClicked = true;
			}
			else if(triggerClicked && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
			{
				triggerClicked = false;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		SteamVR_TrackedObject con = other.GetComponent<SteamVR_TrackedObject>();
		if (con != null)
			touchedController = con;
	}

	void OnTriggerExit(Collider other)
	{
		SteamVR_TrackedObject con = other.GetComponent<SteamVR_TrackedObject>();
		if (con == touchedController)
			touchedController = null;
	}
}
