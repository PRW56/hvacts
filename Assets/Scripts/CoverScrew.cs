﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Placed on screws that fasten the cover of the unit
/// </summary>
public class CoverScrew : Part {
	
	const float timeToFullyUnscrew = .1f;
	float screwedInAmmount; //portion of the screw that is screwed in, 0 - totally unscrewed  1 - totally screwed in
	bool beingScrewed; //lol
	bool unscrewNext; //used to keep track of which direction to screw in

	float screwTimer;
	GameObject screwDriver = null;

	Color originalColor;

	float screwInDistance = .68f;
	
	const float coolDownTime = .1f;
	float coolDownTimer;
	bool preCoolDownExit; //so that the trigger can properly exit before cooldown begins

	// Use this for initialization
	void Start () {
		//find all the other cover screws
		for (int x = 1; x <= 4; x++)
			if ("CoverScrew" + x != this.name)
				AddRelatedPart("CoverScrew" + x);
		//find the unit cover
		AddRelatedPart("UnitCover");

		screwedInAmmount = 1.0f;
		screwTimer = timeToFullyUnscrew;
		beingScrewed = false;
		unscrewNext = true;
		originalColor = GetComponent<MeshRenderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
		if(beingScrewed && screwTimer > 0)
		{
			screwTimer -= Time.deltaTime;
			if(screwTimer <= 0)
			{
				screwedInAmmount = unscrewNext ? 0 : 1;
				if(screwedInAmmount == 1)
					Respond("ScrewIn");
				else
					Respond("Unscrew");
				coolDownTimer = coolDownTime;
				preCoolDownExit = true;
			}
		}
		if (coolDownTimer > 0)
			coolDownTimer -= Time.deltaTime;
	}

	void OnTriggerEnter(Collider other)
	{
		if (coolDownTimer > 0)
			return;
		if (other.name == "ScrewDriverTip")
		{
			beingScrewed = true;
			screwTimer = timeToFullyUnscrew;
			screwDriver = other.gameObject;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (preCoolDownExit == false && coolDownTimer > 0)
			return;
		if (other.name == "ScrewDriverTip")
		{
			beingScrewed = false;
			unscrewNext = !unscrewNext;
			preCoolDownExit = false;
		}
	}

	public bool IsScrewedIn()
	{
		if (screwedInAmmount == 1)
			return true;
		return false;
	}

	public override void Respond(object cause)
	{
		switch((string)cause)
		{
			case "Unscrew":
				//change the screw to look unscrewed
				//GetComponent<MeshRenderer>().material.color = Color.blue;
				transform.localPosition += Vector3.up * screwInDistance;

				//check if all the other screws are unscrewed
				for (int x = 1; x <= 4; x++)
					if (name != "CoverScrew" + x && ((CoverScrew)relatedParts["CoverScrew" + x]).screwedInAmmount == 1)
						return;
				//all the screws are unscrewed, make the cover grabable
				relatedParts["UnitCover"].Respond("Unlock");
				break;
			case "ScrewIn":
				//change the screw to look screwed
				//GetComponent<MeshRenderer>().material.color = originalColor;
				transform.localPosition -= Vector3.up * screwInDistance;
				//lock the unit cover in place
				bool fullLock = true;
				for (int x = 1; x <= 4; x++)
					if (name != "CoverScrew" + x && ((CoverScrew)relatedParts["CoverScrew" + x]).screwedInAmmount == 0)
					{
						fullLock = false;
						break;
					}
				if(fullLock)
					relatedParts["UnitCover"].Respond("FullLock");
				else
					relatedParts["UnitCover"].Respond("Lock");
				break;
			default:
				print(name + " unrecognized Response cause: " + (string)cause);
				break;
		}
	}
}
