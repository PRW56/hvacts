﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Instructor : MonoBehaviour {

	public static Instructor instance;

	string[,] instructions = { {"You are going to be changing the air filter, first locate and kill the power.",
										"Now pickup the screwdriver and unscrew all the screws on the unit door.",
										"Pickup and move the Unit door out of the way.",
										"Remove the used air filter.",
										"Place a New air filter where the old one used to be.",
										"Place the unit cover back on.",
										"Screw the screws on the unit cover back in",
										"Power the machine back on.",
										"Good job!","","","" },
								{"You are going to be relighting the Pilot light, first locate and kill the power.",
										"Now pickup the screwdriver and unscrew all the screws on the unit door.",
										"Pickup and move the Unit door out of the way.",
										"Set the Gas setting to \"Pilot\".",
										"Go ahead and pick up the lighter, then push and hold the Gas button.",
										"Light the pilot light.",
										"Continue to hold the gas button for "+PilotLight.gasTime+" seconds.",
										"Release the gas button",
										"Place the unit cover back on.",
										"Screw the screws on the unit cover back in",
										"Power the machine back on.",
										"Good job!" } };
	string[,] expectedActions = { {"PowerSwitch:PowerOff",
									"UnitCover:Unlock",
									"UnitCover:SnappedOut",
									"UsedAirFilter:SnappedOut",
									"ReplacementAirFilter:SnappedIn",
									"UnitCover:SnappedIn",
									"UnitCover:FullLock",
									"PowerSwitch:PowerOn","","",""},
								{ "PowerSwitch:PowerOff",
									"UnitCover:Unlock",
									"UnitCover:SnappedOut",
									"GasSettingSwitch:SetPilot",
									"GasSwitch:GasOn",
									"PilotLight:Lit",
									"PilotLight:BurnTimeReached",
									"GasSwitch:GasOff",
									"UnitCover:SnappedIn",
									"UnitCover:FullLock",
									"PowerSwitch:PowerOn"} };
	string[] ignoreActions = { "UnitCover:Lock" };
	string[] scenarioPrompts = {"Whenever the air comes on there's a smell. Also the air comes out weaker than it used to.",
								"My heating isn't working. I think its the pilot light..."};
	int scenarioIndex;
	int instructionIndex;
	bool isTutorial;
	Text output;

	public AudioClip correctSound;
	public AudioClip incorrectSound;
	AudioSource audioPlayer;

	public bool levelEnding;
	const float endTime = 15f;
	float endTimer = endTime;

	const float levelTime = 300f;
	float levelTimer;
	int numMistakes;
	bool isWin;
	int score;

	// Use this for initialization
	void Start () {
		instance = this;
		audioPlayer = Camera.main.GetComponent<AudioSource>();
		//only use this script when the current level is not a tutorial
		isTutorial = Player.isTutorial;
		scenarioIndex = Player.currentLevelIndex;
		instructionIndex = 0;
		if (isTutorial)
			transform.GetChild(0).GetComponent<Text>().text = "Instructions";
		else
			transform.GetChild(0).GetComponent<Text>().text = "Discription";
		output = transform.GetChild(1).GetComponent<Text>();
		levelTimer = levelTime;
		isWin = false;
		UpdateDisplay();
		//levelEnding = false;
	}

	//actions should be in the form "NameOfPart:Action"
	public void Inform(string action)
	{
		foreach(string ignore in ignoreActions)
		{
			if (ignore == action)
				return;
		}
		//if expected action, increment index, play good jingle
		if(action == expectedActions[scenarioIndex, instructionIndex])
		{
			
			instructionIndex++;
			if(isTutorial)
			{
				audioPlayer.clip = correctSound;
				audioPlayer.Play();
				UpdateDisplay();
			}
			if (instructions[scenarioIndex, instructionIndex] == "Good job!")
			{
				if(isTutorial == false)
				{
					transform.GetChild(0).GetComponent<Text>().text = "Results";
					isWin = true;
					score = (int)(levelTimer * 100 - numMistakes * 10);
				}
				levelEnding = true;
			}
		}
		else
		{
			numMistakes++;
			if(isTutorial)
			{
				audioPlayer.clip = incorrectSound;
				audioPlayer.Play();
			}
		}
		//if not, play bad jingle
	}

	void UpdateDisplay()
	{
		output.text = (instructionIndex + 1) + ") " + instructions[scenarioIndex, instructionIndex];
	}
	
	// Update is called once per frame
	void Update () {
		if(levelEnding)
		{
			endTimer -= Time.deltaTime;
			if(isTutorial)
			{
				output.text = (instructionIndex + 1) + ") " + instructions[scenarioIndex, instructionIndex];
			}
			else
			{
				output.text = "Number of Mistakes: " + numMistakes + "\nCompletion Time: " + DescribeTime(levelTime - levelTimer) + "\nFinal Score: " + score;
			}
			output.text += "\n\nReturning to Main Menu in " + (int)endTimer + " seconds...";
			if (endTimer <= 0)
				SceneManager.LoadScene("mainScene");
		}
		else if(isTutorial == false)
		{
			levelTimer -= Time.deltaTime;
			output.text = scenarioPrompts[scenarioIndex] + "\n\nTime remaining: " + DescribeTime(levelTimer) + ".";
			if(levelTimer <= 0)
			{
				isWin = false;
				levelEnding = true;
			}
		}
	}

	string DescribeTime(float seconds)
	{
		int minutes = (int)seconds / 60;
		int secs = (int)(seconds - minutes * 60);
		return minutes + " minutes and " + secs + " seconds";
	}
}
