﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ToggleOptions : MonoBehaviour
{

    private SteamVR_TrackedController device;
    private GameObject options, clock;

    // Use this for initialization
    void Start()
    {
        device = GetComponent<SteamVR_TrackedController>();
        device.MenuButtonClicked += Menu;
        options = GameObject.FindGameObjectWithTag("Options");
        clock = GameObject.FindGameObjectWithTag("ClockText");
        options.SetActive(false);
        clock.gameObject.SetActive(false);
    }

    void Menu(object sender, ClickedEventArgs e)
    {
        if (options.activeSelf)
        {
            options.SetActive(false);
            clock.gameObject.SetActive(false);
        }
        else
        {
            options.SetActive(true);
            clock.gameObject.SetActive(true);
        }
    }
}
